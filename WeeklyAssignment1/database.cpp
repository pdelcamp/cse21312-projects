#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "BTree.h"
#include <string>
using namespace std;


struct Data {
	int data;
	int key;

	bool operator<(const Data& rhs) const{
		return (key < rhs.key);
	}
	bool operator>(const Data& rhs) const{
		return (key > rhs.key);
	}
	bool operator==(const Data& rhs) const{
		return (key == rhs.key);
	}
	const Data& operator=(const Data& rhs)	{	
		key = rhs.key;
	}
	friend std::ostream& operator<<(std::ostream& outStream, const Data& printData);
};

/********************************************
 * Function Name  : operator<<
 * Pre-conditions : ostream, const Data&
 * Post-conditions: ostream
 *  
 * This function overloads the << operator 
 * ********************************************/
std::ostream& operator<<(std::ostream& outStream, const Data& printData) {
	outStream << printData.key << ", " << printData.data;
	return outStream;

}

/********************************************
 * Function Name  : main
 * Pre-conditions : int argc, char **argv
 * Post-conditions: int
 *  
 * This is the main driver program for the 
 * BTree function with Data struct 
 * ********************************************/

int main(int argc, char** argv){
	BTree<Data> tree(2);
	Data data1{0};
	int i = 0;
	while(i<1000){
		data1.key = rand()%10;
		data1.data = rand()%10;
		tree.insert(data1);
		i++;
	}
	Data data2{0};
	for(int i = 0; i < 10; i++){
		data2.key = rand()%10;
		data2.data = rand()%10;
		tree.printFoundNodes(data2);
	}
}

