#include <iostream>
#include <map>
#include <iomanip>
#include <fstream>
#include <unordered_map>
#include <list>
#include <vector>
#include <stdio.h>

using namespace std;

//Design a method to find the frequency of occurrences of any given word in a book 

int main(int argc, char * argv[]){
	map<string,int> freq;
	list<string> text;
	string filename = argv[1];
	ifstream ifs;
	if(!ifs){
		cout << "File does not exist. Please, try again." << endl;
		return 1;
	}
	ifs.open(filename);
	vector<string> words;
	string w;
	while(ifs >> w){
		if(w[w.length()-1] < 65)
			w.pop_back();
		for(int i = 0;  i <  w.length() - 1; i++) w[i] = tolower(w[i]);
		words.push_back((w));
	}
	for(int i = 0; i < words.size(); i++) freq[words[i]]++;
	
	cout << "file: "<< filename << endl << endl;
	for(auto iter = freq.cbegin(); iter != freq.cend(); iter++){
		cout << "word: " << iter->first << "\tfrequency: " <<  iter->second << endl;
	}
	
}


	
	
