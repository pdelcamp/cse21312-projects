#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>


int main(int argc, char** argv){

	std::map<std::string, int> ageHashOrdered = {  { "Matthew",30}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38}  };
	std::map<std::string, int>::iterator iterOr;
	
	std::cout <<  "The Ordered ages are: " <<  std::endl;
	
	for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
		std::cout << iterOr->first << " "<< iterOr->second << std::endl;
	}

	std::unordered_map<std::string, int> ageHashUnordered = {  { "Matthew",30}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38}  };	
	std::unordered_map<std::string, int>::iterator iterUn;

	std::cout << "The Unordered ages are: " << std::endl;
	
	for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
		std::cout << iterUn->first << " " << iterUn->second << std::endl;
	}
	 
	std::cout << "The Ordered Example: " << ageHashOrdered["Matthew"] << std::endl;
	std::cout << "The Unordered Example: " << ageHashUnordered["Alfred"] << std::endl;

	return 0;

}

