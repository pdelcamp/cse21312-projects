/**********************************************
 * File: minimal.cpp
 * Author: Patricia del Campillo
 * Email: pdelcamp@nd.edu
 *   
 * Given a sorted (increasing order) array with
 * unique integer elements, write an algorithm
 * to create a binary search tree with minimal height.
 * **********************************************/
#include <iostream>
#include <map>
#include <iomanip>
#include <fstream>

using namespace std;

/*Binary Tree Node*/
struct Node{
	int data;
	Node * left;
	Node * right;
};

/********************************************
 * Function Name  : newNode
 * Pre-conditions : int data
 * Post-conditions: Node *
 *   
 * Creates a new node for the tree
 * ********************************************/

Node * newNode(int data){
	Node * node = new Node();
	node->data  = data;
	node->left  = NULL;
	node->right = NULL;
	
	return node;
}

/********************************************
 * Function Name  : createTree
 * Pre-conditions : int, int, int
 * Post-conditions: Node *
 *   
 * Creates balanced binary search tree from 
 * sorted array.
 * ********************************************/

Node * createTree(int arr[], int start, int end){
	
	//Base Case
	if(start>end) return NULL;
	
	int middle = (start + end)/2;
	Node * root = newNode(arr[middle]);
	
	//Recursive calls
	
	root->left  = createTree(arr, middle + 1, end);
	root->right = createTree(arr, start, middle - 1);

	return root;

}

/********************************************
 * Function Name  : printTraversal 
 * Pre-conditions : Node * node
 * Post-conditions: void
 *   
 * Prints pre order traversal of binary tree
 * ********************************************/

void printTraversal(Node * node){
	if(node == NULL) return;
	
	cout << node->data << " ";
	printTraversal(node->left);
	printTraversal(node->right);
}

/********************************************
 * Function Name  : main 
 * Pre-conditions : int argc, char * argv[]
 * Post-conditions: int
 *   
 * Main driver for the binary tree
 * ********************************************/

int main(int argc, char * argv[]){

	int array[] = {1,2,3,4,5,6,7,8,9,10};
	int n = sizeof(array) / sizeof(array[0]);
	
	Node *root = createTree(array, 0, n-1);
	cout << "Pre-order Traversal of Binary Tree" << endl;
	printTraversal(root);
	cout << endl;

	return 0;
}

