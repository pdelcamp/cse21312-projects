/**********************************************
 * File: path.cpp
 * Author: Patricia del Campillo
 * Email: pdelcamp@nd.edu
 *
 * Check if the path int a graph from one vertex 
 * to another   
 ************************************************/

#include <iostream>
#include <list>

using namespace std;

class Graph {
	int numV;
	list<int> *adj;
public:
	Graph(int numV);
	void addEdge(int f, int s);
	bool isPossible(int s, int e);
	
};

 /********************************************
 * Function Name  : Graph
 * Pre-conditions : int
 * Post-conditions: none
 * 
 * Constructor of the graph class
 *********************************************/

Graph::Graph(int numV){
	this->numV = numV;
	adj = new list<int>[numV];
}
 /********************************************
 * Function Name  : addEdge
 * Pre-conditions : int, int
 * Post-conditions: void
 * 
 * Add an edge to the adjacency list
 *********************************************/
void Graph::addEdge(int f, int s){
	adj[f].push_back(s); 
}
 /********************************************
 * Function Name  : isPossible
 * Pre-conditions : int, int
 * Post-conditions: bool
 * 
 * Function checks if path is poosible
 *********************************************/
bool Graph::isPossible(int s, int e){
	
	if(s == e) return true;

	bool *visited = new bool[numV];
	for(int i = 0; i < numV; i++) visited[i] = false;

	list<int> queue;

	visited[s] = true;
	queue.push_back(s);
	
	list<int>::iterator i;

	while(!queue.empty()){
	
		s = queue.front();
		queue.pop_front();

		for(i = adj[s].begin(); i != adj[s].end(); i++){
			if(*i == e) return true;
			if(!visited[*i]){
				visited[*i] = true;
				queue.push_back(*i);
			}
		}
	}
	return false;
}
 /********************************************
 * Function Name  : main
 * Pre-conditions : none
 * Post-conditions: int
 * 
 * Main driver that creates graph and check paths
 *********************************************/
int main(){
	Graph graph(6);
	graph.addEdge(4,20);
	graph.addEdge(4,5);
	graph.addEdge(20,10);
	graph.addEdge(5,10);
	graph.addEdge(20,30);
	graph.addEdge(30,18);
	graph.addEdge(18,18);

	int start = 5;
	int  end = 18;
	if(graph.isPossible(start,end))
		cout << "\n Path from " << start << " to " << end << "exists." << endl; 
	else 
		cout << "\n The is no path" << endl;
	

	start = 18;
	 end = 4;
	if(graph.isPossible(start,end))
		cout << "\n Path from " << start << " to " << end << endl;
	else
		cout << "There is no path from " << start << "to" << end << endl;

	return 0;

}
