Input string = 'abcdcbefgfeh'
Palindromic Partitions: 
start	i	(i-start+1)	str.substr(start, i-start+1)	IsPartitionPalindrome?
0	0	1		a				 is a Palindrome!
1	1	1		b				 is a Palindrome!
2	2	1		c				 is a Palindrome!
3	3	1		d				 is a Palindrome!
4	4	1		c				 is a Palindrome!
5	5	1		b				 is a Palindrome!
6	6	1		e				 is a Palindrome!
7	7	1		f				 is a Palindrome!
8	8	1		g				 is a Palindrome!
9	9	1		f				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
9	10	2		fe				
9	11	3		feh				
8	9	2		gf				
8	10	3		gfe				
8	11	4		gfeh				
7	8	2		fg				
7	9	3		fgf				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
7	10	4		fgfe				
7	11	5		fgfeh				
6	7	2		ef				
6	8	3		efg				
6	9	4		efgf				
6	10	5		efgfe				 is a Palindrome!
11	11	1		h				 is a Palindrome!
6	11	6		efgfeh				
5	6	2		be				
5	7	3		bef				
5	8	4		befg				
5	9	5		befgf				
5	10	6		befgfe				
5	11	7		befgfeh				
4	5	2		cb				
4	6	3		cbe				
4	7	4		cbef				
4	8	5		cbefg				
4	9	6		cbefgf				
4	10	7		cbefgfe				
4	11	8		cbefgfeh				
3	4	2		dc				
3	5	3		dcb				
3	6	4		dcbe				
3	7	5		dcbef				
3	8	6		dcbefg				
3	9	7		dcbefgf				
3	10	8		dcbefgfe				
3	11	9		dcbefgfeh				
2	3	2		cd				
2	4	3		cdc				 is a Palindrome!
5	5	1		b				 is a Palindrome!
6	6	1		e				 is a Palindrome!
7	7	1		f				 is a Palindrome!
8	8	1		g				 is a Palindrome!
9	9	1		f				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
9	10	2		fe				
9	11	3		feh				
8	9	2		gf				
8	10	3		gfe				
8	11	4		gfeh				
7	8	2		fg				
7	9	3		fgf				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
7	10	4		fgfe				
7	11	5		fgfeh				
6	7	2		ef				
6	8	3		efg				
6	9	4		efgf				
6	10	5		efgfe				 is a Palindrome!
11	11	1		h				 is a Palindrome!
6	11	6		efgfeh				
5	6	2		be				
5	7	3		bef				
5	8	4		befg				
5	9	5		befgf				
5	10	6		befgfe				
5	11	7		befgfeh				
2	5	4		cdcb				
2	6	5		cdcbe				
2	7	6		cdcbef				
2	8	7		cdcbefg				
2	9	8		cdcbefgf				
2	10	9		cdcbefgfe				
2	11	10		cdcbefgfeh				
1	2	2		bc				
1	3	3		bcd				
1	4	4		bcdc				
1	5	5		bcdcb				 is a Palindrome!
6	6	1		e				 is a Palindrome!
7	7	1		f				 is a Palindrome!
8	8	1		g				 is a Palindrome!
9	9	1		f				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
9	10	2		fe				
9	11	3		feh				
8	9	2		gf				
8	10	3		gfe				
8	11	4		gfeh				
7	8	2		fg				
7	9	3		fgf				 is a Palindrome!
10	10	1		e				 is a Palindrome!
11	11	1		h				 is a Palindrome!
10	11	2		eh				
7	10	4		fgfe				
7	11	5		fgfeh				
6	7	2		ef				
6	8	3		efg				
6	9	4		efgf				
6	10	5		efgfe				 is a Palindrome!
11	11	1		h				 is a Palindrome!
6	11	6		efgfeh				
1	6	6		bcdcbe				
1	7	7		bcdcbef				
1	8	8		bcdcbefg				
1	9	9		bcdcbefgf				
1	10	10		bcdcbefgfe				
1	11	11		bcdcbefgfeh				
0	1	2		ab				
0	2	3		abc				
0	3	4		abcd				
0	4	5		abcdc				
0	5	6		abcdcb				
0	6	7		abcdcbe				
0	7	8		abcdcbef				
0	8	9		abcdcbefg				
0	9	10		abcdcbefgf				
0	10	11		abcdcbefgfe				
0	11	12		abcdcbefgfeh				
Total Partition Combinations = 9
a b c d c b e f g f e h 
a b c d c b e fgf e h 
a b c d c b efgfe h 
a b cdc b e f g f e h 
a b cdc b e fgf e h 
a b cdc b efgfe h 
a bcdcb e f g f e h 
a bcdcb e fgf e h 
a bcdcb efgfe h 
------------
