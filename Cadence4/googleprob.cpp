//write code that will find the shortest words for one license plate

#include <iostream>
#include <cctype>
#include <string>
#include <algorithm> 
#include <map>
#include <fstream>
#include <iomanip>
#include <vector>

using namespace std;


 /********************************************
 * Function Name  : isWord
 * Pre-conditions : string,string
 * Post-conditions: bool
 * Checks if word is valid/complete
 *********************************************/

 bool   isWord(string licensePlate, string word){

	map<char, int> w;
	for(auto& letter : word) 
		w[letter]++;

	transform(licensePlate.begin(),licensePlate.end(), licensePlate.begin(), ::toupper);

	for(auto& letter : licensePlate){
		if(isalpha(letter)){
			w[letter]--;
			if(w[letter] < 0)
				return false;
		}
	}
	return true;
 }

 /********************************************
 * Function Name  : shortestWord
 * Pre-conditions : string, vector<string>
 * Post-conditions: string
 * Finds shortest word
 *********************************************/

string shortestWord(string licensePlate, vector<string>& words){

	vector<string> sol;
	for(auto w : words){
		if(isWord(licensePlate,w))
			sol.push_back(w);
	}

	if(sol.size() > 0){
		stable_sort(sol.begin(),sol.end(), [](string a, string b){ return a.size() < b.size(); });
		return sol[0];
	}

	return NULL;
}

 /********************************************
 * Function Name  : main
 * Pre-conditions : int, char*
 * Post-conditions: int
 * Main driver function. Reads in file and calls
 * other functions.
 *********************************************/

int main(int argc, char * argv[]){
	string filename = argv[1];
	ifstream ifs;
	if(!ifs){
		cout << "File does not exist. Please try again." << endl;
		return 1;
	}
	ifs.open(filename);
	vector<string> words;
	string w;
	while(ifs >> w) {
		if(w[w.length()-1] < 65)
			w.pop_back();
		
		words.push_back(w);
	}
	vector<string> plates = {"RT 123SO","LET10EE0","RCT100SA","AQ 10S0K","AEI 1O2U3"};
	cout << "The shortest words from these plates are: " << endl;
	for(int i = 0; i < plates.size(); i++){
		string shortWord = shortestWord(plates[i],words);
		cout << i+1 << ". " << plates[i] << "\t" << shortWord << endl;
	}
	return 0;
}
	
	
